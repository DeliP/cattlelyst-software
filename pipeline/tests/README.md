# Tests folder
This directory contains:
- the scripts and jupyter notebooks used to obtained the results presented in the thesis "Pipeline for the identification of metabolic engineering strategies and the suggestion of Biobrick parts "
- some additional output file with other data relevant for the results

### The content of this repository is associated to thesis' sections as listed below:

- 3.3.1 VALIDATION01_itaconic_acid_ecoli.ipynb
- 3.3.2 VALIDATION_putida_xylose.ipynb
- 3.3.3 VALIDATION_putida_arabinose.ipynb
- 3.2.1 
    - script_analysis_lactate_server.py
    - role_of_transporters_for_lactate_production_in_Model3.ipynb
    - all .csv files
- 3.2.2 
    - THERMODYNAMICS_formate_Model1.ipynb 
    - THERMODYNAMICS_formate_Model2.ipynb
    - THERMODYNAMICS_lactate_Model1.ipynb
    - THERMODYNAMICS_lactate_Model1_results.ipynb 
    - all .tsv files  
- 3.2.3 SCORES_methane_to_lactate.ipynb
- 3.2.4 CODON_HARMONIZATION_results_including_Lcasei_seq.ipynb
