#### Cattlelyst WUR iGEM team 2021 contribution to the Bioparts tools

- a pipeline in Python with a Jupyter Notebook interface suggesting metabolic engineering strategies particularly suitable for bioremediation and bioeconomy objectives
- the biopart wikibase, a database of biobricks that reorganised the iGEM registry in a machine readable manner.